# TeamMonitoringBack

## Aims

This project exposes Rest API to use MonitorTeamFront.
It Allows configuration and follow up on your team's worklife.

## Structure

### api

Contains API documentation such as swagger contract

### build

Contains all tools to build up the project

### cmd

Contains main entry points to the application

## Parameters

Those parameters are available at this time:  

| name        | description                         | DEFAULT   |  
| ----------- | ----------------------------------- | --------- |  
| LISTEN_PORT | the port the application listens to | 8080      |  
| MONGO_HOST  | the host for the mongodb server     | localhost |  
| MONGO_PORT  | the port for the mongodb server     | 27017     |  

## Build & Run

```bash
go build cmd/main
LISTEN_PORT=8080 ./main
```
