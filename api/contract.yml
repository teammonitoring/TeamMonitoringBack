openapi: "3.0.0"
info:
  description: "This describes the TeamMonitoring backend API"
  version: 0.0.0
  title: TeamMonitoringBack
  license:
    name: MIT
paths:
  /health-check:
    get:
      tags:
        - health check
      responses:
        204:
          description: "OK"
  /teams:
    get:
      tags:
        - team
      summary: "Get list of teams"
      description: "Returns the list of all registered teams"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/Team"
    post:
      tags:
        - team
      summary: "Add new team"
      description: "Create a new team from the team sent in the body. Note: the id will be ignored and replace."
      requestBody:
        description: "The team to be created"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Team"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Team"
  /teams/{teamId}:
    get:
      tags:
        - team
      summary: "Get team identified by teamId"
      description: "Fetch team information"
      parameters:
        - name: teamId
          in: path
          required: true
          description: The id of the team to retrieve
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Team"
        404:
          description: "The id does not exists"
    delete:
      tags:
        - team
      summary: "Delete team identified by teamId"
      description: "Delete team information"
      parameters:
        - name: teamId
          in: path
          required: true
          description: The id of the team to retrieve
          schema:
            type: string
      responses:
        204:
          description: "successful operation"
        404:
          description: "The id does not exists"
    put:
      tags:
        - team
      summary: "Update team identified by teamId"
      description: "update team information"
      parameters:
        - name: teamId
          in: path
          required: true
          description: The id of the team to retrieve
          schema:
            type: string
      requestBody:
        description: "The team to be updated"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Team"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Team"
        400:
          description: "Error in the input data"
        404:
          description: "The id does not exists"
  /teams/{teamId}/projects:
    get:
      tags:
        - team
      summary: "Get team's projects"
      description: "Gather projects information from connectors and return it"
      parameters:
        - name: teamId
          in: path
          required: true
          description: The id of the team to retrieve
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/Project"
        404:
          description: "The id does not exists"
  /teams/{teamId}/merge-requests:
    get:
      tags:
        - team
      summary: "Get team's merge requests"
      description: "Gather merge requests information from connectors and return it"
      parameters:
        - name: teamId
          in: path
          required: true
          description: The id of the team to retrieve
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/MergeRequest"
        404:
          description: "The id does not exists"
  /probes:
    get:
      tags:
        - probe
      summary: "Get list of probes"
      description: "Returns the list of all registered probes"
      parameters:
        - name: teamId
          in: "query"
          description: Allow to filter probes by team id
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/HttpProbe"
    post:
      tags:
        - probe
      summary: "Add new probe"
      description: "Create a new probe from the probe sent in the body. Note: 1) the id will be ignored and replace 2) the probe will be deleted if the team is deleted. "
      requestBody:
        description: "The probe to be created"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/HttpProbe"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HttpProbe"
        400:
          description: "Wrong parameters input, check teamId exists and url has a correct format"
  /probes/{probeId}:
    get:
      tags:
        - probe
      summary: "Get probe identified by probeId"
      description: "Fetch probe information"
      parameters:
        - name: probeId
          in: path
          required: true
          description: The id of the probe to retrieve
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HttpProbe"
        404:
          description: "The id does not exists"
    delete:
      tags:
        - probe
      summary: "Delete probe identified by probeId"
      description: "Delete team information"
      parameters:
        - name: probeId
          in: path
          required: true
          description: The id of the probe to delete
          schema:
            type: string
      responses:
        204:
          description: "successful operation"
        404:
          description: "The id does not exists"
    put:
      tags:
        - probe
      summary: "Update team identified by probeId"
      description: "update team information"
      parameters:
        - name: probeId
          in: path
          required: true
          description: The id of the probe to update
          schema:
            type: string
      requestBody:
        description: "The probe to be updated"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/HttpProbe"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HttpProbe"
        400:
          description: "Error in the input data"
        404:
          description: "The id does not exists"
  /connectors:
    get:
      tags:
        - connector
      summary: "Get list of connectors"
      description: "Returns the list of all registered connectors"
      parameters:
        - name: teamId
          in: "query"
          description: Allow to filter connectors by team internal reference
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/Connector"
    post:
      tags:
        - connector
      summary: "Add new connector"
      description: "Create a new connector from the connector sent in the body. Note: the id will be ignored and replace."
      requestBody:
        description: "The connector to be created"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Connector"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Connector"
  /connectors/{connectorId}:
    get:
      tags:
        - connector
      summary: "Get connector identified by connector id"
      description: "Fetch connector information"
      parameters:
        - name: connectorId
          in: path
          required: true
          description: The id of the connector to retrieve
          schema:
            type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Connector"
        404:
          description: "The id does not exists"
    delete:
      tags:
        - connector
      summary: "Delete connector identified by connectorId"
      description: "Delete team information"
      parameters:
        - name: connectorId
          in: path
          required: true
          description: The id of the connector to delete
          schema:
            type: string
      responses:
        204:
          description: "successful operation"
        404:
          description: "The id does not exists"
    put:
      tags:
        - connector
      summary: "Update team identified by connectorId"
      description: "update team information"
      parameters:
        - name: connectorId
          in: path
          required: true
          description: The id of the connector to update
          schema:
            type: string
      requestBody:
        description: "The connector to be updated"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Connector"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Connector"
        400:
          description: "Error in the input data"
        404:
          description: "The id does not exists"
  /connectors/types:
    get:
      tags:
      - connector
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  type: "string"
components:
  schemas:
    Team:
      type: "object"
      properties:
        id:
          type: "string"
          format: "uuid"
          description: "Internal reference of the team"
        name:
          type: "string"
          description: "Name to be displayed on overviews"
        description:
          type: "string"
          description: "Short description of what the team does"
        avatar:
          type: "string"
          description: "Link to an image representing the team"
    HttpProbe:
      type: "object"
      properties:
        id:
          type: "string"
          format: "uuid"
          description: "Internal reference of the probe"
        name:
          type: "string"
          description: "Name of the probe to be displayed in overview"
        tag:
          type: "string"
          description: "Tag is used to group probes"
        url:
          type: "string"
          format: "uri"
          description: "The url to scout"
        teamId:
          type: "string"
          format: "uuid"
          description: "Associated team internal reference"
    Connector:
      type: "object"
      properties:
        id:
          type: "string"
          format: "uuid"
          description: "Internal reference of the connector"
        type:
          type: "string"
          description: "Type of the connector, plans are to support gitlab, github and bitbucket"
        url:
          type: "string"
          description: "URL of the server."
        privateToken:
          type: "string"
          description: "Private token used to authenticate to the api."
        teamId:
          type: "string"
          description: "Associated team internal reference."
        teamReference:
          type: "string"
          description: "Associated team external reference (groups for gitlab, team for bitbucket)."
    Project:
      type: "object"
      properties:
        name:
          type: "string"
        avatar:
          type: "string"
        description:
          type: "string"
        lastUpdateDate:
          type: "string"
    MergeRequest:
      type: "object"
      properties:
        title:
          type: "string"
        description:
          type: "string"
        state:
          type: "string"
        createdAt:
          type: "string"
