package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/httphandler"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/usecases"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// WebApp Represent the web application
type WebApp struct {
	TeamsController     *httphandler.TeamsController
	HTTPProbeController *httphandler.HTTPProbeController
	ConnectorController *httphandler.ConnectorController
}

// Serve start the http server
func (webApp *WebApp) Serve(port int) {
	listen := fmt.Sprintf(":%d", port)

	// wirering up
	router := mux.NewRouter()
	router.HandleFunc("/v1/health-check", httphandler.RespondNoContent).Methods(http.MethodGet)
	router.HandleFunc("/v1/teams", webApp.TeamsController.GetTeams).Methods(http.MethodGet)
	router.HandleFunc("/v1/teams", webApp.TeamsController.CreateTeam).Methods(http.MethodPost)
	router.HandleFunc("/v1/teams/{id}", webApp.TeamsController.GetTeam).Methods(http.MethodGet)
	router.HandleFunc("/v1/teams/{id}", webApp.TeamsController.DeleteTeam).Methods(http.MethodDelete)
	router.HandleFunc("/v1/teams/{id}", webApp.TeamsController.UpdateTeam).Methods(http.MethodPut)
	router.HandleFunc("/v1/teams/{id}/projects", webApp.TeamsController.GetTeamProjects).Methods(http.MethodGet)
	router.HandleFunc("/v1/teams/{id}/merge-requests", webApp.TeamsController.GetTeamMergeRequests).Methods(http.MethodGet)

	router.HandleFunc("/v1/probes", webApp.HTTPProbeController.GetHTTPProbes).Methods(http.MethodGet)
	router.HandleFunc("/v1/probes", webApp.HTTPProbeController.CreateHTTPProbe).Methods(http.MethodPost)
	router.HandleFunc("/v1/probes/{id}", webApp.HTTPProbeController.GetHTTPProbe).Methods(http.MethodGet)
	router.HandleFunc("/v1/probes/{id}", webApp.HTTPProbeController.DeleteHTTPProbe).Methods(http.MethodDelete)
	router.HandleFunc("/v1/probes/{id}", webApp.HTTPProbeController.UpdateHTTPProbe).Methods(http.MethodPut)

	router.HandleFunc("/v1/connectors", webApp.ConnectorController.GetConnectors).Methods(http.MethodGet)
	router.HandleFunc("/v1/connectors", webApp.ConnectorController.CreateConnector).Methods(http.MethodPost)
	router.HandleFunc("/v1/connectors/types", webApp.ConnectorController.GetAvailableTypes).Methods(http.MethodGet)
	router.HandleFunc("/v1/connectors/{id}", webApp.ConnectorController.GetConnector).Methods(http.MethodGet)
	router.HandleFunc("/v1/connectors/{id}", webApp.ConnectorController.DeleteConnector).Methods(http.MethodDelete)
	router.HandleFunc("/v1/connectors/{id}", webApp.ConnectorController.UpdateConnector).Methods(http.MethodPut)

	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	loggedRouter := handlers.LoggingHandler(os.Stdout, handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(router))

	log.Printf("Starting server on %s", listen)
	log.Fatal(http.ListenAndServe(listen, loggedRouter))
}

func main() {
	listenPort, err := strconv.Atoi(utils.GetEnv("LISTEN_PORT", "8080"))
	if err != nil {
		log.Fatal("Error parsing configuration port")
	}
	mongoHost := utils.GetEnv("MONGO_HOST", "localhost")
	mongoPort, err := strconv.Atoi(utils.GetEnv("MONGO_PORT", "27017"))
	if err != nil {
		log.Fatal("Error parsing configuration port")
	}

	client := connectToMongo(mongoHost, mongoPort)
	defer client.Disconnect(context.TODO())

	// TODO: pass db name as parameters
	database := client.Database("test")

	// TODO: replace this with proper IOC
	// TECHNICAL
	teamRepository := persistence.NewMongoTeamRepository(database)
	httpProbeRepository := persistence.NewMongoHTTPProbeRepository(database)
	connectorRepository := persistence.NewMongoConnectorRepository(database)

	// FUNCTIONAL
	createTeamService := usecases.NewCreateTeamService(teamRepository)
	deleteTeamService := usecases.NewDeleteTeamService(teamRepository, httpProbeRepository, connectorRepository)
	createHTTPProbeService := usecases.NewCreateHTTPProbeService(teamRepository, httpProbeRepository)
	createConnectorService := usecases.NewCreateConnectorService(teamRepository, connectorRepository)
	listProjectService := usecases.NewListProjectService(connectorRepository)
	listMergeRequestService := usecases.NewListMergeRequestService(connectorRepository)

	// HOST
	connectorController := httphandler.NewConnectorController(connectorRepository, createConnectorService, createConnectorService)
	httpProbeController := httphandler.NewHTTPProbeController(httpProbeRepository, createHTTPProbeService, createHTTPProbeService)
	teamsController := httphandler.NewTeamsController(createTeamService, deleteTeamService, teamRepository, listProjectService, listMergeRequestService)

	webApp := &WebApp{teamsController, httpProbeController, connectorController}
	webApp.Serve(listenPort)
}

func connectToMongo(mongoHost string, mongoPort int) *mongo.Client {
	clientOptions := options.Client()
	// TODO: pass connection info via parameters
	clientOptions.ApplyURI(fmt.Sprintf("mongodb://%s:%d", mongoHost, mongoPort))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	return client
}
