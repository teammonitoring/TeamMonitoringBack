package httphandler

import (
	"net/http"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/usecases"
)

// HTTPProbeController controller for httpprobe operations
type HTTPProbeController struct {
	httpProbeStorage persistence.HTTPProbeStorage
	httpProbeCreater usecases.HTTPProbeCreator
	httpProbeUpdater usecases.HTTPProbeUpdater
}

// NewHTTPProbeController factory for HTTPProbeController
func NewHTTPProbeController(
	httpProbeStorage persistence.HTTPProbeStorage,
	httpProbeCreator usecases.HTTPProbeCreator,
	httpProbeUpdater usecases.HTTPProbeUpdater) *HTTPProbeController {
	return &HTTPProbeController{httpProbeStorage, httpProbeCreator, httpProbeUpdater}
}

// GetHTTPProbes returns all probes
func (httpProbeController *HTTPProbeController) GetHTTPProbes(w http.ResponseWriter, r *http.Request) {
	teamID := r.FormValue("teamId")
	if len(teamID) > 0 {
		RespondJSON(w, http.StatusOK, httpProbeController.httpProbeStorage.FindByTeamID(teamID))
		return
	}
	RespondJSON(w, http.StatusOK, httpProbeController.httpProbeStorage.FindAll())
}

// GetHTTPProbe returns the probe
func (httpProbeController *HTTPProbeController) GetHTTPProbe(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")

	probe := httpProbeController.httpProbeStorage.FindByID(id)
	if probe != nil {
		RespondJSON(w, http.StatusOK, probe)
		return
	}
	RespondNotFound(w, r)
}

// DeleteHTTPProbe deletes probe
func (httpProbeController *HTTPProbeController) DeleteHTTPProbe(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")

	err := httpProbeController.httpProbeStorage.DeleteByID(id)

	if err == nil {
		RespondNoContent(w, r)
		return
	}
	RespondNotFound(w, r)
}

// UpdateHTTPProbe updates probe
func (httpProbeController *HTTPProbeController) UpdateHTTPProbe(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")
	var httpProbe models.HTTPProbe
	serializeErr := DeserializeRequest(r, &httpProbe)

	if serializeErr != nil {
		RespondInternalServerError(w, serializeErr)
		return
	}

	if id == httpProbe.ID {
		newHTTPProbe, err := httpProbeController.httpProbeUpdater.UpdateHTTPProbe(&httpProbe)

		if err != nil {
			RespondBadRequest(w, r)
			return
		}

		RespondJSON(w, http.StatusOK, newHTTPProbe)
	}
	RespondBadRequest(w, r)
}

// CreateHTTPProbe updates probe
func (httpProbeController *HTTPProbeController) CreateHTTPProbe(w http.ResponseWriter, r *http.Request) {
	var httpProbe models.HTTPProbe
	serializeErr := DeserializeRequest(r, &httpProbe)

	if serializeErr != nil {
		RespondInternalServerError(w, serializeErr)
		return
	}

	newHTTPProbe, err := httpProbeController.httpProbeCreater.CreateHTTPProbe(&httpProbe)

	if err != nil {
		RespondBadRequest(w, r)
		return
	}

	RespondJSON(w, http.StatusOK, newHTTPProbe)
}
