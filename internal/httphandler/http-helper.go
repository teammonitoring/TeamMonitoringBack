package httphandler

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/utils"
)

// RespondJSON Helper that write json response
func RespondJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// RespondNoContent returns status code no content
func RespondNoContent(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

// RespondOk returns status code OK
func RespondOk(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

// RespondNotFound returns status code OK
func RespondNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}

// RespondBadRequest returns status code OK
func RespondBadRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
}

// GetQueryParam returns query param
func GetQueryParam(r *http.Request, key string) string {
	vars := mux.Vars(r)
	return vars[key]
}

// DeserializeRequest Simplify request deserialization
func DeserializeRequest(r *http.Request, v interface{}) error {
	return utils.DeserializeBody(r.Body, v)
}

// RespondInternalServerError write 500 status
func RespondInternalServerError(w http.ResponseWriter, err error) {
	RespondInternalServerError(w, err)
}
