package httphandler

import (
	"net/http"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/usecases"
)

// ConnectorController controller for connector operations
type ConnectorController struct {
	connectorStorage persistence.ConnectorStorage
	connectorCreator usecases.ConnectorCreator
	connectorUpdater usecases.ConnectorUpdater
}

// NewConnectorController factory for ConnectorController
func NewConnectorController(
	connectorStorage persistence.ConnectorStorage,
	connectorCreator usecases.ConnectorCreator,
	connectorUpdater usecases.ConnectorUpdater) *ConnectorController {
	return &ConnectorController{connectorStorage, connectorCreator, connectorUpdater}
}

// GetConnectors returns all probes
func (connectorController *ConnectorController) GetConnectors(w http.ResponseWriter, r *http.Request) {
	teamID := r.FormValue("teamId")
	if len(teamID) > 0 {
		RespondJSON(w, http.StatusOK, connectorController.connectorStorage.FindByTeamID(teamID))
		return
	}
	RespondJSON(w, http.StatusOK, connectorController.connectorStorage.FindAll())
}

// GetConnector returns the probe
func (connectorController *ConnectorController) GetConnector(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")

	probe := connectorController.connectorStorage.FindByID(id)
	if probe != nil {
		RespondJSON(w, http.StatusOK, probe)
		return
	}
	RespondNotFound(w, r)
}

// DeleteConnector deletes probe
func (connectorController *ConnectorController) DeleteConnector(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")

	err := connectorController.connectorStorage.DeleteByID(id)

	if err == nil {
		RespondNoContent(w, r)
		return
	}
	RespondNotFound(w, r)
}

// UpdateConnector updates probe
func (connectorController *ConnectorController) UpdateConnector(w http.ResponseWriter, r *http.Request) {
	id := GetQueryParam(r, "id")
	var connector models.Connector
	serializeErr := DeserializeRequest(r, &connector)

	if serializeErr != nil {
		RespondInternalServerError(w, serializeErr)
		return
	}

	if connector.Type != "gitlab" {
		RespondBadRequest(w, r)
		return
	}

	if id == connector.ID {
		newConnector, err := connectorController.connectorUpdater.UpdateConnector(&connector)

		if err != nil {
			RespondBadRequest(w, r)
			return
		}

		RespondJSON(w, http.StatusOK, newConnector)
	}
	RespondBadRequest(w, r)
}

// CreateConnector updates probe
func (connectorController *ConnectorController) CreateConnector(w http.ResponseWriter, r *http.Request) {
	var connector models.Connector
	serializeErr := DeserializeRequest(r, &connector)

	if serializeErr != nil {
		RespondInternalServerError(w, serializeErr)
		return
	}

	if connector.Type != "gitlab" {
		RespondBadRequest(w, r)
		return
	}

	newConnector, err := connectorController.connectorCreator.CreateConnector(&connector)

	if err != nil {
		RespondBadRequest(w, r)
		return
	}

	RespondJSON(w, http.StatusOK, newConnector)
}

// GetAvailableTypes returns available connector types
func (connectorController *ConnectorController) GetAvailableTypes(w http.ResponseWriter, r *http.Request) {
	response := [1]string{"gitlab"}
	RespondJSON(w, http.StatusOK, response)
}
