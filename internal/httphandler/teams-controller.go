package httphandler

import (
	"net/http"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/usecases"
)

type teamStorage interface {
	FindByID(id string) *models.Team
	FindAll() []*models.Team
	Update(team *models.Team) (*models.Team, error)
}

// TeamsController Handles request on the teams api
type TeamsController struct {
	teamCreator            usecases.TeamCreator
	teamDeleter            usecases.TeamDeleter
	teamStorage            persistence.TeamStorage
	teamProjectLoader      usecases.TeamProjectLoader
	teamMergeRequestLoader usecases.TeamMergeRequestLoader
}

// NewTeamsController Factory for TeamsController
func NewTeamsController(
	teamCreator usecases.TeamCreator,
	teamDeleter usecases.TeamDeleter,
	teamStorage persistence.TeamStorage,
	teamProjectLoader usecases.TeamProjectLoader,
	teamMergeRequestLoader usecases.TeamMergeRequestLoader) *TeamsController {
	return &TeamsController{teamCreator, teamDeleter, teamStorage, teamProjectLoader, teamMergeRequestLoader}
}

// GetTeams returns all teams
func (teamsController *TeamsController) GetTeams(w http.ResponseWriter, r *http.Request) {
	RespondJSON(w, http.StatusOK, teamsController.teamStorage.FindAll())
}

// GetTeam return the team identified by id
func (teamsController *TeamsController) GetTeam(w http.ResponseWriter, r *http.Request) {
	teamID := GetQueryParam(r, "id")

	team := teamsController.teamStorage.FindByID(teamID)

	if team != nil {
		RespondJSON(w, http.StatusOK, team)
		return
	}
	RespondNotFound(w, r)
}

// DeleteTeam return the team identified by id
func (teamsController *TeamsController) DeleteTeam(w http.ResponseWriter, r *http.Request) {
	teamID := GetQueryParam(r, "id")

	err := teamsController.teamDeleter.DeleteTeam(teamID)

	if err == nil {
		RespondNoContent(w, r)
		return
	}
	RespondNotFound(w, r)
}

// UpdateTeam update and then return the team identified by id
func (teamsController *TeamsController) UpdateTeam(w http.ResponseWriter, r *http.Request) {
	teamID := GetQueryParam(r, "id")
	var team models.Team
	err := DeserializeRequest(r, &team)

	if err != nil {
		RespondInternalServerError(w, err)
		return
	}

	if teamID != team.ID {
		RespondBadRequest(w, r)
		return
	}

	savedTeam, err := teamsController.teamStorage.Update(&team)

	if savedTeam == nil {
		RespondNotFound(w, r)
		return
	}

	RespondJSON(w, http.StatusOK, savedTeam)
}

// CreateTeam Creates a new team
func (teamsController *TeamsController) CreateTeam(w http.ResponseWriter, r *http.Request) {
	var team models.Team
	serializeErr := DeserializeRequest(r, &team)

	if serializeErr != nil {
		RespondInternalServerError(w, serializeErr)
		return
	}

	createdTeam, err := teamsController.teamCreator.CreateTeam(&team)

	if err != nil {
		RespondInternalServerError(w, err)
		return
	}

	RespondJSON(w, http.StatusOK, createdTeam)
}

// GetTeamProjects Creates a new team
func (teamsController *TeamsController) GetTeamProjects(w http.ResponseWriter, r *http.Request) {
	teamID := GetQueryParam(r, "id")

	projects, err := teamsController.teamProjectLoader.LoadProjects(teamID)

	if err != nil {
		RespondInternalServerError(w, err)
		return
	}

	RespondJSON(w, http.StatusOK, projects)
}

// GetTeamMergeRequests Creates a new team
func (teamsController *TeamsController) GetTeamMergeRequests(w http.ResponseWriter, r *http.Request) {
	teamID := GetQueryParam(r, "id")

	mergeRequests, err := teamsController.teamMergeRequestLoader.LoadMergeRequests(teamID)

	if err != nil {
		RespondInternalServerError(w, err)
		return
	}

	RespondJSON(w, http.StatusOK, mergeRequests)
}
