package usecases

import (
	"errors"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
)

// ConnectorCreator creates connector
type ConnectorCreator interface {
	CreateConnector(connector *models.Connector) (*models.Connector, error)
}

// ConnectorUpdater updates connector
type ConnectorUpdater interface {
	UpdateConnector(connector *models.Connector) (*models.Connector, error)
}

// CreateConnectorService creates connector
type CreateConnectorService struct {
	teamStorage      persistence.TeamStorage
	connectorStorage persistence.ConnectorStorage
}

// NewCreateConnectorService Factory for CreateConnectorService
func NewCreateConnectorService(teamStorage persistence.TeamStorage, connectorStorage persistence.ConnectorStorage) *CreateConnectorService {
	return &CreateConnectorService{teamStorage, connectorStorage}
}

// CreateConnector create http connector use cases
func (createConnectorService *CreateConnectorService) CreateConnector(connector *models.Connector) (*models.Connector, error) {
	team := createConnectorService.teamStorage.FindByID(connector.TeamID)

	if team == nil {
		return nil, errors.New("Wrong team id")
	}

	newConnector := models.NewConnector()
	newConnector.Type = connector.Type
	newConnector.URL = connector.URL
	newConnector.PrivateToken = connector.PrivateToken
	newConnector.TeamID = connector.TeamID
	newConnector.TeamReference = connector.TeamReference

	err := createConnectorService.connectorStorage.Save(newConnector)

	if err != nil {
		return nil, err
	}
	return newConnector, nil
}

// UpdateConnector create http connector use cases
func (createConnectorService *CreateConnectorService) UpdateConnector(connector *models.Connector) (*models.Connector, error) {
	team := createConnectorService.teamStorage.FindByID(connector.TeamID)

	if team == nil {
		return nil, errors.New("Wrong team id")
	}

	_, err := createConnectorService.connectorStorage.Update(connector)

	if err != nil {
		return nil, err
	}
	return connector, nil
}
