package usecases

import (
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/adapters"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
)

// TeamMergeRequestLoader loads project
type TeamMergeRequestLoader interface {
	LoadMergeRequests(teamID string) ([]*models.MergeRequest, error)
}

// ListMergeRequestService list MergeRequests of a team
type ListMergeRequestService struct {
	connectorStorage persistence.ConnectorStorage
}

// NewListMergeRequestService factory
func NewListMergeRequestService(connectorStorage persistence.ConnectorStorage) *ListMergeRequestService {
	return &ListMergeRequestService{connectorStorage}
}

// LoadMergeRequests returns a list of MergeRequest
func (listMergeRequestService *ListMergeRequestService) LoadMergeRequests(teamID string) ([]*models.MergeRequest, error) {
	connectors := listMergeRequestService.connectorStorage.FindByTeamID(teamID)
	mergeRequests := make([]*models.MergeRequest, 0)
	for _, connector := range connectors {
		if connector.Type == "gitlab" {
			loadedMergeRequests, err := adapters.GetGitlabMergeRequests(connector)

			if err != nil {
				return nil, err
			}

			for _, mergeRequest := range loadedMergeRequests {
				mergeRequests = append(mergeRequests, mergeRequest)
			}
		}
	}
	return mergeRequests, nil
}
