package usecases

import (
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
)

// TeamCreator creates team
type TeamCreator interface {
	CreateTeam(team *models.Team) (*models.Team, error)
}

// CreateTeamService service to create save teams
type CreateTeamService struct {
	teamStorage persistence.TeamStorage
}

// NewCreateTeamService factory for creating service
func NewCreateTeamService(teamStorage persistence.TeamStorage) *CreateTeamService {
	return &CreateTeamService{teamStorage}
}

// CreateTeam creates and save teams
func (createTeamService *CreateTeamService) CreateTeam(team *models.Team) (*models.Team, error) {
	createdTeam := models.NewTeam()
	createdTeam.Name = team.Name
	createdTeam.Description = team.Description
	createdTeam.Avatar = team.Avatar
	saveError := createTeamService.teamStorage.Save(createdTeam)
	if saveError != nil {
		return nil, saveError
	}

	return team, nil
}
