package usecases

import (
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/adapters"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
)

// TeamProjectLoader loads project
type TeamProjectLoader interface {
	LoadProjects(teamID string) ([]*models.Project, error)
}

// ListProjectService list projects of a team
type ListProjectService struct {
	connectorStorage persistence.ConnectorStorage
}

// NewListProjectService factory
func NewListProjectService(connectorStorage persistence.ConnectorStorage) *ListProjectService {
	return &ListProjectService{connectorStorage}
}

// LoadProjects returns a list of project
func (listProjectService *ListProjectService) LoadProjects(teamID string) ([]*models.Project, error) {
	connectors := listProjectService.connectorStorage.FindByTeamID(teamID)
	projects := make([]*models.Project, 0)
	for _, connector := range connectors {
		if connector.Type == "gitlab" {
			loadedProjects, err := adapters.GetGitlabProjects(connector)

			if err != nil {
				return nil, err
			}

			for _, project := range loadedProjects {
				projects = append(projects, project)
			}
		}
	}
	return projects, nil
}
