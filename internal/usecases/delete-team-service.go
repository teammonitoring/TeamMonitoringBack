package usecases

import "gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"

// TeamDeleter deletes team
type TeamDeleter interface {
	DeleteTeam(id string) error
}

// DeleteTeamService execute team deletion
type DeleteTeamService struct {
	teamStorage      persistence.TeamStorage
	httpProbeStorage persistence.HTTPProbeStorage
	connectorStorage persistence.ConnectorStorage
}

// NewDeleteTeamService Factory for DeleteTeamService
func NewDeleteTeamService(
	teamStorage persistence.TeamStorage,
	httpProbeStorage persistence.HTTPProbeStorage,
	connectorStorage persistence.ConnectorStorage) *DeleteTeamService {
	return &DeleteTeamService{teamStorage, httpProbeStorage, connectorStorage}
}

// DeleteTeam deletes a team and its dependencies
func (deleteTeamService *DeleteTeamService) DeleteTeam(id string) error {
	probes := deleteTeamService.httpProbeStorage.FindByTeamID(id)
	for _, probe := range probes {
		err := deleteTeamService.httpProbeStorage.DeleteByID(probe.ID)
		if err != nil {
			return err
		}
	}

	connectors := deleteTeamService.connectorStorage.FindByTeamID(id)
	for _, connector := range connectors {
		err := deleteTeamService.connectorStorage.DeleteByID(connector.ID)
		if err != nil {
			return err
		}
	}

	return deleteTeamService.teamStorage.DeleteByID(id)
}
