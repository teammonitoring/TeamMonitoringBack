package usecases

import (
	"errors"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/persistence"
)

// HTTPProbeCreator creates HTTPProbe
type HTTPProbeCreator interface {
	CreateHTTPProbe(probe *models.HTTPProbe) (*models.HTTPProbe, error)
}

// HTTPProbeUpdater updates HTTPProbe
type HTTPProbeUpdater interface {
	UpdateHTTPProbe(probe *models.HTTPProbe) (*models.HTTPProbe, error)
}

// CreateHTTPProbeService creates probe
type CreateHTTPProbeService struct {
	teamStorage      persistence.TeamStorage
	httpProbeStorage persistence.HTTPProbeStorage
}

// NewCreateHTTPProbeService Factory for CreateHTTPProbeService
func NewCreateHTTPProbeService(teamStorage persistence.TeamStorage, httpProbeStorage persistence.HTTPProbeStorage) *CreateHTTPProbeService {
	return &CreateHTTPProbeService{teamStorage, httpProbeStorage}
}

// CreateHTTPProbe create http probe use cases
func (createHTTPProbeService *CreateHTTPProbeService) CreateHTTPProbe(probe *models.HTTPProbe) (*models.HTTPProbe, error) {
	team := createHTTPProbeService.teamStorage.FindByID(probe.TeamID)

	if team == nil {
		return nil, errors.New("Wrong team id")
	}

	newProbe := models.NewHTTPProbe()
	newProbe.Name = probe.Name
	newProbe.TeamID = probe.TeamID
	newProbe.URL = probe.URL
	newProbe.Tag = probe.Tag

	err := createHTTPProbeService.httpProbeStorage.Save(newProbe)

	if err != nil {
		return nil, err
	}
	return newProbe, nil
}

// UpdateHTTPProbe create http probe use cases
func (createHTTPProbeService *CreateHTTPProbeService) UpdateHTTPProbe(probe *models.HTTPProbe) (*models.HTTPProbe, error) {
	team := createHTTPProbeService.teamStorage.FindByID(probe.TeamID)

	if team == nil {
		return nil, errors.New("Wrong team id")
	}

	_, err := createHTTPProbeService.httpProbeStorage.Update(probe)

	if err != nil {
		return nil, err
	}
	return probe, nil
}
