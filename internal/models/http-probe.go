package models

import "github.com/google/uuid"

// HTTPProbe represent and http probe
type HTTPProbe struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	URL    string `json:"url"`
	TeamID string `json:"teamId"`
	Tag    string `json:"tag"`
}

// NewHTTPProbe factory for HTTPProbe
func NewHTTPProbe() *HTTPProbe {
	return &HTTPProbe{ID: uuid.New().String()}
}
