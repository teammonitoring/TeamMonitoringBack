package models

// MergeRequest represent a merge request
type MergeRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	State       string `json:"state"`
	CreatedAt   string `json:"createdAt"`
}
