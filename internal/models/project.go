package models

// Project Represents a project
type Project struct {
	Name           string `json:"name"`
	Avatar         string `json:"avatar"`
	Description    string `json:"description"`
	LastUpdateDate string `json:"lastUpdateDate"`
}
