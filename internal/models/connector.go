package models

import "github.com/google/uuid"

// Connector represent a connection to another data provider
type Connector struct {
	ID            string `json:"id"`
	Type          string `json:"type"`
	URL           string `json:"url"`
	PrivateToken  string `json:"privateToken"`
	TeamID        string `json:"teamId"`
	TeamReference string `json:"teamReference"`
}

// NewConnector factory for Connector
func NewConnector() *Connector {
	return &Connector{ID: uuid.New().String()}
}
