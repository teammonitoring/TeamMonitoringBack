package models

import (
	"github.com/google/uuid"
)

// Team represents a team
type Team struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Avatar      string `json:"avatar"`
}

// NewTeam creates a Team
func NewTeam() *Team {
	return &Team{ID: uuid.New().String()}
}
