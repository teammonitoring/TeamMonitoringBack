package utils

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// DeserializeBody Simplify body  deserialization
func DeserializeBody(body io.ReadCloser, v interface{}) error {
	b, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		return err
	}

	return json.Unmarshal(b, &v)
}
