package utils

import "os"

// GetEnv returns the environment variable
func GetEnv(name string, defaultValue string) string {
	var tmp = os.Getenv(name)
	if len(tmp) > 0 {
		return tmp
	}
	return defaultValue
}
