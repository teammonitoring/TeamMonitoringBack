package adapters

import (
	"fmt"
	"net/http"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/utils"
)

type gitlabProject struct {
	Name           string `json:"name"`
	Avatar         string `json:"avatar_url"`
	Description    string `json:"description"`
	LastUpdateDate string `json:"last_activity_at"`
}

type gitlabMergeRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	State       string `json:"state"`
	CreatedAt   string `json:"created_at"`
}

// GetGitlabProjects loads gitlab projects
func GetGitlabProjects(connector *models.Connector) ([]*models.Project, error) {
	url := fmt.Sprintf("%s/api/v4//groups/%s/projects?private_token=%s", connector.URL, connector.TeamReference, connector.PrivateToken)
	resp, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	var loadedProjects []gitlabProject
	err = utils.DeserializeBody(resp.Body, &loadedProjects)

	if err != nil {
		return nil, err
	}

	projects := make([]*models.Project, len(loadedProjects))
	for index, project := range loadedProjects {
		projects[index] = &models.Project{
			Name:           project.Name,
			Avatar:         project.Avatar,
			Description:    project.Description,
			LastUpdateDate: project.LastUpdateDate}
	}
	return projects, nil
}

// GetGitlabMergeRequests loads gitlab mergerequest
func GetGitlabMergeRequests(connector *models.Connector) ([]*models.MergeRequest, error) {
	url := fmt.Sprintf("%s/api/v4//groups/%s/merge_requests?view=simple&private_token=%s", connector.URL, connector.TeamReference, connector.PrivateToken)
	resp, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	var loadedMergeRequests []gitlabMergeRequest
	err = utils.DeserializeBody(resp.Body, &loadedMergeRequests)

	if err != nil {
		return nil, err
	}

	mergeRequests := make([]*models.MergeRequest, len(loadedMergeRequests))
	for index, mergeRequest := range loadedMergeRequests {
		mergeRequests[index] = &models.MergeRequest{
			Title:       mergeRequest.Title,
			State:       mergeRequest.State,
			Description: mergeRequest.Description,
			CreatedAt:   mergeRequest.CreatedAt}
	}
	return mergeRequests, nil
}
