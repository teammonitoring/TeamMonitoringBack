package persistence

import (
	"context"
	"log"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// ConnectorStorage provides connector data manipulation
type ConnectorStorage interface {
	FindByTeamID(id string) []*models.Connector
	DeleteByID(id string) error
	FindByID(id string) *models.Connector
	Save(connector *models.Connector) error
	Update(connector *models.Connector) (*models.Connector, error)
	FindAll() []*models.Connector
}

// MongoConnectorRepository Connector repository for mongo backend
type MongoConnectorRepository struct {
	Collection *mongo.Collection
}

// NewMongoConnectorRepository factory
func NewMongoConnectorRepository(database *mongo.Database) *MongoConnectorRepository {
	return &MongoConnectorRepository{database.Collection("connectors")}
}

// Save saves Connector
func (mongoConnectorRepository *MongoConnectorRepository) Save(connector *models.Connector) error {
	_, err := mongoConnectorRepository.Collection.InsertOne(context.TODO(), connector)
	return err
}

// FindByID retreive connector
func (mongoConnectorRepository *MongoConnectorRepository) FindByID(id string) *models.Connector {
	filter := getFilterForID(id)
	var result models.Connector
	err := mongoConnectorRepository.Collection.FindOne(context.TODO(), filter).Decode(&result)

	if err == nil {
		return &result
	}
	return nil
}

// FindAll returns all connector
func (mongoConnectorRepository *MongoConnectorRepository) FindAll() []*models.Connector {
	var results []*models.Connector

	cur, err := mongoConnectorRepository.Collection.Find(context.TODO(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.Connector
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	return results
}

// FindByTeamID returns all connector associated to team
func (mongoConnectorRepository *MongoConnectorRepository) FindByTeamID(id string) []*models.Connector {
	filter := getFilterForTeamID(id)

	var results []*models.Connector

	cur, err := mongoConnectorRepository.Collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.Connector
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}
	return results
}

// DeleteByID deletes connector
func (mongoConnectorRepository *MongoConnectorRepository) DeleteByID(id string) error {
	filter := getFilterForID(id)
	_, err := mongoConnectorRepository.Collection.DeleteOne(context.TODO(), filter)
	return err
}

// Update returns all stored connectors
func (mongoConnectorRepository *MongoConnectorRepository) Update(connector *models.Connector) (*models.Connector, error) {
	// TODO: Find better way to update
	filter := getFilterForID(connector.ID)
	_, err := mongoConnectorRepository.Collection.UpdateOne(context.TODO(), filter, bson.D{
		bson.E{Key: "$set", Value: bson.D{
			bson.E{Key: "id", Value: connector.ID},
			bson.E{Key: "type", Value: connector.Type},
			bson.E{Key: "url", Value: connector.URL},
			bson.E{Key: "privatetoken", Value: connector.PrivateToken},
			bson.E{Key: "teamid", Value: connector.TeamID},
			bson.E{Key: "teamreference", Value: connector.TeamReference}}}})
	return connector, err
}
