package persistence

import (
	"context"
	"log"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// HTTPProbeStorage interface for probes manipulation
type HTTPProbeStorage interface {
	FindByTeamID(id string) []*models.HTTPProbe
	DeleteByID(id string) error
	FindByID(id string) *models.HTTPProbe
	Save(httpProbe *models.HTTPProbe) error
	Update(httpProbe *models.HTTPProbe) (*models.HTTPProbe, error)
	FindAll() []*models.HTTPProbe
}

// MongoHTTPProbeRepository HTTPProbe repository for mongo backend
type MongoHTTPProbeRepository struct {
	Collection *mongo.Collection
}

// NewMongoHTTPProbeRepository factory
func NewMongoHTTPProbeRepository(database *mongo.Database) *MongoHTTPProbeRepository {
	return &MongoHTTPProbeRepository{database.Collection("http-probes")}
}

// Save saves httpProbe
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) Save(httpProbe *models.HTTPProbe) error {
	_, err := mongoHTTPProbeRepository.Collection.InsertOne(context.TODO(), httpProbe)
	return err
}

// FindByID retreive httpProbe
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) FindByID(id string) *models.HTTPProbe {
	filter := getFilterForID(id)
	var result models.HTTPProbe
	err := mongoHTTPProbeRepository.Collection.FindOne(context.TODO(), filter).Decode(&result)

	if err == nil {
		return &result
	}
	return nil
}

// FindAll returns all httpProbe
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) FindAll() []*models.HTTPProbe {
	var results []*models.HTTPProbe

	cur, err := mongoHTTPProbeRepository.Collection.Find(context.TODO(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.HTTPProbe
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	return results
}

// FindByTeamID returns all httpProbe associated to team
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) FindByTeamID(id string) []*models.HTTPProbe {
	filter := getFilterForTeamID(id)

	var results []*models.HTTPProbe

	cur, err := mongoHTTPProbeRepository.Collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.HTTPProbe
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}
	return results
}

// DeleteByID deletes httpProbe
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) DeleteByID(id string) error {
	filter := getFilterForID(id)
	_, err := mongoHTTPProbeRepository.Collection.DeleteOne(context.TODO(), filter)
	return err
}

// Update returns all stored httpProbes
func (mongoHTTPProbeRepository *MongoHTTPProbeRepository) Update(httpProbe *models.HTTPProbe) (*models.HTTPProbe, error) {
	// TODO: Find better way to update
	filter := getFilterForID(httpProbe.ID)
	_, err := mongoHTTPProbeRepository.Collection.UpdateOne(context.TODO(), filter, bson.D{
		bson.E{Key: "$set", Value: bson.D{
			bson.E{Key: "id", Value: httpProbe.ID},
			bson.E{Key: "name", Value: httpProbe.Name},
			bson.E{Key: "url", Value: httpProbe.URL},
			bson.E{Key: "teamid", Value: httpProbe.TeamID},
			bson.E{Key: "tag", Value: httpProbe.Tag}}}})
	return httpProbe, err
}
