package persistence

import "go.mongodb.org/mongo-driver/bson"

func getFilterForID(id string) interface{} {
	return bson.D{bson.E{Key: "id", Value: id}}
}

func getFilterForTeamID(teamID string) interface{} {
	return bson.D{bson.E{Key: "teamid", Value: teamID}}
}
