package persistence

import (
	"context"
	"log"

	"gitlab.com/TeamMonitoring/TeamMonitoringBack/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// TeamStorage provides interface for team collection
type TeamStorage interface {
	DeleteByID(id string) error
	FindByID(id string) *models.Team
	Save(team *models.Team) error
	Update(team *models.Team) (*models.Team, error)
	FindAll() []*models.Team
}

// MongoTeamRepository Team repository for mongo backend
type MongoTeamRepository struct {
	Collection *mongo.Collection
}

// Save saves team
func (mongoTeamRepository *MongoTeamRepository) Save(team *models.Team) error {
	_, err := mongoTeamRepository.Collection.InsertOne(context.TODO(), team)
	return err
}

// FindByID retreive team
func (mongoTeamRepository *MongoTeamRepository) FindByID(id string) *models.Team {
	filter := getFilterForID(id)
	var result models.Team
	err := mongoTeamRepository.Collection.FindOne(context.TODO(), filter).Decode(&result)

	if err == nil {
		return &result
	}
	return nil
}

// FindAll returns all team
func (mongoTeamRepository *MongoTeamRepository) FindAll() []*models.Team {
	findOptions := options.Find()

	var results []*models.Team

	cur, err := mongoTeamRepository.Collection.Find(context.TODO(), bson.D{}, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem models.Team
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	return results
}

// DeleteByID deletes team
func (mongoTeamRepository *MongoTeamRepository) DeleteByID(id string) error {
	filter := getFilterForID(id)
	_, err := mongoTeamRepository.Collection.DeleteOne(context.TODO(), filter)
	return err
}

// Update returns all stored teams
func (mongoTeamRepository *MongoTeamRepository) Update(team *models.Team) (*models.Team, error) {
	// TODO: Find better way to update
	filter := getFilterForID(team.ID)
	_, err := mongoTeamRepository.Collection.UpdateOne(context.TODO(), filter, bson.D{
		bson.E{Key: "$set", Value: bson.D{
			bson.E{Key: "id", Value: team.ID},
			bson.E{Key: "name", Value: team.Name},
			bson.E{Key: "description", Value: team.Description},
			bson.E{Key: "avatar", Value: team.Avatar}}}})
	return team, err
}

// NewMongoTeamRepository factory
func NewMongoTeamRepository(database *mongo.Database) *MongoTeamRepository {
	return &MongoTeamRepository{database.Collection("teams")}
}
